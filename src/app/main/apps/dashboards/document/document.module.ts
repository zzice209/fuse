import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DocumentComponent} from 'app/main/apps/dashboards/document/document.component';
import {ValidInputComponent} from 'app/main/apps/dashboards/valid-input/valid-input.component';
import {RouterModule, Routes} from '@angular/router';
import {AnalyticsDashboardService} from '@app/main/apps/dashboards/analytics/analytics.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
    {
        path: '**',
        component: DocumentComponent,
        resolve: {
            data: AnalyticsDashboardService
        }
    }
];

@NgModule({
    declarations: [DocumentComponent, ValidInputComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule
    ],
    providers   : [
        AnalyticsDashboardService,
    ]
})
export class DocumentModule {
}
