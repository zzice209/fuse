import {Component, OnInit, Input} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PasswordMatchVaildator } from '../valid-input/passwordmatch';


import {fromEvent, interval, of} from 'rxjs';

import {from, timer, zip} from 'rxjs';
import {pluck, map, debounceTime, distinctUntilChanged, catchError} from 'rxjs/operators';


@Component({
    selector: 'app-document',
    templateUrl: './document.component.html',
    styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {

    constructor(private passwordMatchValidator: PasswordMatchVaildator) {
    }

    signupForm = new FormGroup({
        name: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', [Validators.required, Validators.minLength(6)]),
        confirmPassword: new FormControl('', [Validators.required, Validators.minLength(6)])

    }, {validators: this.passwordMatchValidator.validate})//This is the custom validator

    isFormError(){
        const {errors} = this.signupForm;
        const passwordTouched = this.signupForm.get('password').touched;
        const confirmPasswordTouched = this.signupForm.get('confirmPassword').touched;
        return errors && passwordTouched && confirmPasswordTouched;
    }



    ngOnInit() {

    }


}
