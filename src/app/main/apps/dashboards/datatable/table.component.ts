import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';
import {TableDashboardService} from '@app/main/apps/dashboards/datatable/table.service';

export interface UserData {
    id: string;
    name: string;
    progress: string;
    color: string;
}

@Component({
    selector: 'datatable-dashboard',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss'],
})
export class TableDashboardComponent implements OnInit {
    displayedColumns: string[] = ['id', 'name', 'progress', 'color'];
    dataSource: MatTableDataSource<UserData>;
    users: any;
    errors: string;

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) sort: MatSort;

    horizontalPosition: MatSnackBarHorizontalPosition = 'right';
    verticalPosition: MatSnackBarVerticalPosition = 'top';

    constructor(
        private _tableDashboardService: TableDashboardService,
        private _snackBar: MatSnackBar
    ) {
    }

    ngOnInit(): void {
        this._tableDashboardService.getPersons().subscribe((data) => {
                this.users = data;
                this.dataSource = new MatTableDataSource(this.users);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
                this.openSnackBar('Call api success', 'Close', 'green-snackbar');
            },
            error1 => {
                this.errors = error1;
                this.openSnackBar('Call api fail', 'Close', 'red-snackbar');
            });
    }

    applyFilter(filterValue: string): void {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    openSnackBar(message: string, action: string, className: string): void {
        this._snackBar.open(message, action, {
            duration: 2000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            panelClass: [className]
        });
    }

}
