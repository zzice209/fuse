import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import API_URL from '@app/api/api.url';

@Injectable({
    providedIn: 'root'
})
export class TableDashboardService {

    constructor(private http: HttpClient) {
    }

    getPersons(): Observable<any> {
        return this.http.get(API_URL.PERSONS);
    }
}
