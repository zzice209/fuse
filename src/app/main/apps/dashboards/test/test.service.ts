import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import API_URL from '@app/api/api.url';

@Injectable({
    providedIn: 'root'
})
export class TestDashboardService {

    constructor(private http: HttpClient) {
    }

    getPosts(): Observable<any> {
        return this.http.get(API_URL.POSTS);
    }
    createPost(post): Observable<any> {
        return this.http.post(API_URL.POSTS, post);
    }
    updatePost(post): Observable<any> {
        return this.http.put(API_URL.POSTS + post.id, post);
    }
    deletePost(id: number): Observable<any> {
        return this.http.delete(API_URL.POSTS + id);
    }
}
