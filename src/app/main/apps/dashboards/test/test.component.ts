import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';
// import { AnalyticsDashboardService } from 'app/main/apps/dashboards/analytics/analytics.service';
import {TestDashboardService} from 'app/main/apps/dashboards/test/test.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'test-dashboard',
    templateUrl: './test.component.html',
    styleUrls: ['./test.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class TestDashboardComponent implements OnInit {
    posts: any;
    newTodo: any;
    form: FormGroup;

    constructor(
        // private _analyticsDashboardService: AnalyticsDashboardService,
        private _testDashboardService: TestDashboardService,
        private _formBuilder: FormBuilder
    ) {

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */

    ngOnInit(): void {
        this.form = this._formBuilder.group({
            id: [''],
            userId: ['', Validators.required],
            title : ['', Validators.required],
            body  : ['', Validators.required],
        });
        this.getList();
    }

    getList(): void {
        this._testDashboardService.getPosts().subscribe((data => {
            this.posts = data;
        }));
    }

    addTodo(): void {
        // console.log(this.newTodo);
        this._testDashboardService.createPost({
            userId: this.form.controls.userId.value,
            // id: Math.floor(Math.random() * Math.floor(1000000)),
            id: this.form.controls.id.value,
            title: this.form.controls.title.value,
            body: this.form.controls.body.value
        }).subscribe((data => {
            console.log(data);
        }));
        // this._httpClient.post('http://localhost:3000/people', {name: this.newTodo});
        this.getList();
    }

    updatePost(): void {
        this._testDashboardService.updatePost({
            id: this.form.controls.id.value,
            userId: this.form.controls.userId.value,
            title: this.form.controls.title.value,
            body: this.form.controls.body.value
        }).subscribe((data => {
            console.log(data);
        }));
        this.getList();
    }

    deletePost(id): void {
        this._testDashboardService.deletePost(id).subscribe((data => {
            console.log(data);
        }));
        console.log(typeof id);
        this.getList();
    }
}

