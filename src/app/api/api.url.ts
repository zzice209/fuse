const API_URL = {
    POSTS: 'http://localhost:3000/posts/',
    PERSONS: 'http://localhost:3000/persons/'
}
export default API_URL;
